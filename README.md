# elsa_testing

Testing repository for the elsa recon toolbox

## Summary

This repository is used for two purposes. First, it's used to tests the install and subproject functionality of [elsa](https://gitlab.lrz.de/IP/elsa). Second, it's a repository for example usage of our code.

We decided to move both here, as it would be strange to have exampels in the main repository and here. But because this is also to test the functionality of installing and using elsa, as an installed library the setup is a little more unusual. For this reson we have all the find_package calls, rather deep down in the project tree, rather than on the top level. And for this reason we many examples don't "share" the dependencies, as they sometimes need some kind of special variation of the setup.

## How to setup

Create a build director.

In build directory run 

```
conan install ..
```

If conan can't find a a binary for (e.g. spdlog and fmtlib) you settings run:

```
conan install .. --build=fmt,spdlog 
```