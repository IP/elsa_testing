cmake_minimum_required(VERSION 3.18 FATAL_ERROR)

project(
    elsa_testing
    VERSION 0.1
    DESCRIPTION "sampels for elsa recon toolbox"
    LANGUAGES CXX
)

# ------------ general setup -----------
# ------------

option(ELSA_CUDA_VECTOR "Build elsa with GPU DataContainer support and default" OFF)
set(ELSA_BRANCH "master" CACHE STRING "branch from which to download examples from")

set(ELSA_INSTALL_DIR "" CACHE PATH "Path hint to elsa install dir")

# add our cmake modules under cmake/
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

# Include CPM dependency manager
include(CPM)

# ------------ Setup general dependcies ------------
# ------------
message(STATUS "Pulling examples files from elsa branch: ${ELSA_BRANCH}")

find_package(OpenMP)

CPMAddPackage("gh:gabime/spdlog@1.7.0")

if(Spdlog_ADDED)
    set(spdlog_INCLUDE_DIR ${Spdlog_SOURCE_DIR}/include)
endif()

# setup Eigen Library
CPMAddPackage(
    NAME eigen3
    GIT_REPOSITORY https://gitlab.com/libeigen/eigen
    GIT_TAG 3.4.0
    DOWNLOAD_ONLY YES # Eigen's CMakelists are not intended for library use
)

# Add the Eigen library target to link against
if(eigen3_ADDED)
    add_library(Eigen3::Eigen INTERFACE IMPORTED)
    target_include_directories(Eigen3::Eigen INTERFACE ${eigen3_SOURCE_DIR})

    set(EIGEN3_INCLUDE_DIR ${eigen3_SOURCE_DIR})
endif()

# Add Quickvec after Eigen3
if(${ELSA_CUDA_VECTOR})
    include(CheckLanguage)
    check_language(CUDA)
    find_package(CUDA 11.0)
    if(CMAKE_CUDA_COMPILER AND (CUDA_VERSION VERSION_GREATER 11))
        enable_language(CUDA)
    else()
        message(FATAL_ERROR "CUDA_VECTOR language support not found")
    endif()

    find_package(CUDAToolkit)
    if(CUDAToolkit_FOUND)
        message(STATUS "CUDA Toolkit -- FOUND")
    else()
        message(STATUS "CUDA Toolkit -- NOT FOUND")
    endif()

    find_package(Thrust 1.13 REQUIRED CONFIG HINTS "${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}/../lib/cmake/thrust")
else()
    CPMAddPackage(NAME thrust GITHUB_REPOSITORY NVIDIA/thrust GIT_TAG 1.17.2)
    # elsa should be able to link against the backends used there!
    set(THRUST_DEVICE_SYSTEM CPP)

    thrust_create_target(Thrust FROM_OPTIONS)
endif()

# ------------ add subfolders ------------
# ------------

# the elsa library
add_subdirectory(examples)
