# * Try to find elsa lib
#
# This module supports requiring a minimum version, e.g. you can do
# `find_package(elsa 0.5.0)` to require version 0.5.0 or newer.
#
# Once done this will define
#
# * elsa_FOUND - system has eigen lib with correct version
# * elsa_INCLUDE_DIR - the eigen include directory
# * elsa_CONFIG_DIR - the eigen include directory
#
# and all installed elsa modules, which usually include:
#
# * elsa::all
# * elsa::core
# * elsa::functionals
# * elsa::generators
# * elsa::io
# * elsa::logging
# * elsa::ml
# * elsa::operators
# * elsa::problems
# * elsa::projectors
# * elsa::proximity_operators
# * elsa::solvers
#
# And if elsa was build with CUDA support, also
#
# * elsa::projectors_cuda
#
# will be included
#
# This module reads hints about search locations from the following environment
# variables:
#
# * elsa_ROOT
# * elsa_ROOT_DIR
# * elsa_INSTALL_DIR
# * ELSA_INSTALL_DIR
#
# as well as the following CMake variables:
#
# * elsa_INSTALL_DIR
# * ELSA_INSTALL_DIR

if(NOT elsa_FOUND)
  # search first if an elsaConfig.cmake is available in the system, if
  # successful this would set EIGEN3_INCLUDE_DIR and the rest of the script will
  # work as usual
  find_package(elsa ${elsa_FIND_VERSION} NO_MODULE QUIET)

  # Search for the configuration files such as elsaConfig.cmake
  if(NOT elsa_CONFIG_DIR)
    include(CMakeFindDependencyMacro)
    find_path(
      elsa_CONFIG_DIR
      NAMES elsaConfig.cmake
      HINTS ENV elsa_ROOT
            ENV elsa_ROOT_DIR
            ENV ELSA_INSTALL_DIR
            ENV elsa_INSTALL_DIR
            ${elsa_INSTALL_DIR}
            ${ELSA_INSTALL_DIR}
      PATHS ${CMAKE_INSTALL_PREFIX}/include
      PATH_SUFFIXES elsa lib/elsa lib/cmake/elsa)

    find_dependency(elsa REQUIRED HINTS ${elsa_CONFIG_DIR})
  endif()

  # Search for the include directory where elsa.h is
  if(NOT elsa_INCLUDE_DIR)
    find_path(
      elsa_INCLUDE_DIR
      NAMES elsa.h
      HINTS ENV elsa_ROOT
            ENV elsa_ROOT_DIR
            ENV ELSA_INSTALL_DIR
            ENV elsa_INSTALL_DIR
            ${elsa_INSTALL_DIR}
            ${ELSA_INSTALL_DIR}
      PATHS ${CMAKE_INSTALL_PREFIX}/include ${CMAKE_INSTALL_PREFIX}/include/elsa
      PATH_SUFFIXES elsa include include/elsa)
  endif()

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(elsa DEFAULT_MSG elsa_INCLUDE_DIR
                                    elsa_CONFIG_DIR)

  mark_as_advanced(elsa_INCLUDE_DIR)
  mark_as_advanced(elsa_CONFIG_DIR)
endif()

if(NOT elsa_FOUND)
  message(FATAL_ERROR "Could not find elsa, try passing elsa_INSTALL_DIR")
endif()

if(elsa_FOUND AND NOT TARGET elsa::all)
  message(FATAL_ERROR "elsa::all not found")
endif()
