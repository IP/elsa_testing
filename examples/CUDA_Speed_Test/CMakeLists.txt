include(CheckLanguage)

check_language(CUDA)

if(CMAKE_CUDA_COMPILER)
  find_package(elsa REQUIRED)
  file(
    DOWNLOAD
    https://gitlab.lrz.de/IP/elsa/-/raw/${ELSA_BRANCH}/examples/speed_test.cpp
    ${CMAKE_CURRENT_LIST_DIR}/speed_test.cpp)

  add_executable(example_cuda_speed speed_test.cpp)
  target_link_libraries(example_cuda_speed elsa::all)
  target_compile_features(example_cuda_speed PUBLIC cxx_std_17)

  if(ELSA_CUDA_VECTOR)
    set_source_files_properties(speed_test.cpp PROPERTIES LANGUAGE CUDA)

    # CMake does not pass -fopenmp to the host compiler when building with NVCC,
    # so set manually
    target_compile_options(example_cuda_speed PRIVATE -Xcompiler=-fopenmp)
  endif()

  add_dependencies(examples example_cuda_speed)
else()
  message(STATUS "CUDA Speed Test: CUDA support disabled")
endif()
